<?php

namespace {
    require 'vendor/autoload.php';

    use PSLib\Configuration\ElasticConfig;
    use PSLib\Utils\ElasticSearch\ElasticClient;
    use PSLib\Utils\ElasticSearch\ElasticClientBulkBuilder;

    $host   = 'db.local.gl';
    $port   = '9200';
    $prefix = 'debug_';

    $config = new ElasticConfig($host, $port, $prefix);
    $client = new ElasticClient($config, 'test', 'attachments');
    $builder = new ElasticClientBulkBuilder();
}

namespace PSLib\Configuration {
    class ElasticConfig
    {
        private $host;
        private $port;
        private $prefix;

        public function __construct($host, $port, $prefix)
        {
            $this->host = $host;
            $this->port = $port;
            $this->prefix = $prefix;
        }

        public function getPrefix()
        {
            return $this->prefix;
        }

        public function getPort()
        {
            return $this->port;
        }

        public function getHost()
        {
            return $this->host;
        }

        public function getIndexName($index)
        {
            return $this->getPrefix() . $index;
        }
    }

}

namespace PSLib\Utils\ElasticSearch {
    use Exception;
    use Symfony\Component\HttpFoundation\Response;
    use PSLib\Configuration\ElasticConfig;
    use PSLib\Utils\Lists\Seq;

    class ElasticClient
    {
        private $host;
        private $port;
        private $indexName;
        private $typeName;
        private $curl = null;

        public function __construct(ElasticConfig $config, $indexName, $typeName)
        {
            $this->host = $config->getHost();
            $this->port = $config->getPort();
            $this->indexName = $config->getIndexName($indexName);
            $this->typeName = $typeName;
        }

        public function createIndexAndMapping($jsonIndexMapping, $jsonTypeMappingAsString)
        {
            $this->rawPost($this->indexName . '/', [Response::HTTP_OK], $jsonIndexMapping);
            $this->rawPost($this->indexName . '/' . $this->typeName . '/_mapping', [Response::HTTP_OK], $jsonTypeMappingAsString);
        }

        public function createMapping($jsonTypeMappingAsString)
        {
            $this->rawPost($this->indexName . '/' . $this->typeName . '/_mapping', [Response::HTTP_OK], $jsonTypeMappingAsString);
        }

        public function nuke()
        {
            $this->rawDelete($this->indexName . '/', [Response::HTTP_OK, Response::HTTP_NOT_FOUND]);
        }

        public function get($id)
        {
            return $this->rawGet($this->getTypeUrl(), $id);
        }

        public function getMany(Array $ids)
        {
            if (count($ids) < 1) {
                return ['docs' => []];
            }

            $get = $this->getStringFromArray($ids);

            return $this->rawPost($this->getTypeUrl() . '/_mget', [Response::HTTP_OK], '{"ids":[' . $get . ']}');
        }

        public function search($body)
        {
            $response = $this->rawPost($this->getTypeUrl().'/_search', [Response::HTTP_OK], $body);
            if (!isset($response['hits']))
                throw new \InvalidArgumentException("Query failed: " . $json);
            if ($response['hits']['total'] === 0) {
                return ['meta' => ['total_rows' => 0], 'data' => []];
            }
            if (!isset($response['hits']['hits'])) {
                return ['meta' => ['total_rows' => 0], 'data' => []];
            }
            $hits = Seq::ofArray($response['hits']['hits'])
                ->map(function($d) { return $d['_source']; })
                ->toArray();
            return [
                'meta' => ['total' => $response['hits']['total']],
                'data' => $hits
            ];
        }

        public function put($id, $documentBody)
        {
            $this->rawPut($this->getTypeUrl() . '/' . (string) $id, [Response::HTTP_OK, Response::HTTP_CREATED], $documentBody);
        }

        public function paritalUpdate($id, $partialDocumentBody)
        {
            $this->rawPost(
                    $this->getTypeUrl() . '/' . (string) $id . '/_update', [Response::HTTP_OK], '{"doc":' . $partialDocumentBody . '}'
            );
        }

        public function updateMany(ElasticClientBulkBuilder $builder)
        {
            $body = $builder->toBatchBody($this->indexName, $this->typeName);
            if ($body) {
                $results = $this->rawPost('_bulk?refresh=true', [Response::HTTP_OK], $body);
                foreach ($results['items'] as $res) {
                    $this->checkStatus($res);
                }
            }
        }

        private function getStringFromArray(array $values)
        {
            $string = '';
            foreach ($values as $value) {
                if ($string) {
                    $string .= ',';
                }
                $string .= '"' . (string) $value . '"';
            }

            return $string;
        }

        private function checkStatus(array $result)
        {
            $validStatusCodes = [Response::HTTP_OK, Response::HTTP_CREATED];

            $status = null;
            if (isset($result['index'])) {
                $index = $result['index'];
            } else if (isset($result['delete'])) {
                $index = $result['delete'];
                $validStatusCodes[] = Response::HTTP_NOT_FOUND;
            } else {
                $arrayKeys = implode(', ', array_keys($result));
                throw new Exception("Unknown result response from ElasticSearch. Response keys:".$arrayKeys);
            }

            if (!in_array($index['status'], $validStatusCodes)) {

                throw new Exception('Unable to save '.$index["_id"].' in '.$index['_index'].'/'.$index['_type'].' with status code:'.$index['status'].print_r($result,true));
            }
        }

        private function getTypeUrl()
        {
            return $this->indexName . '/' . $this->typeName;
        }

        private function rawGet($url, $id)
        {
            $curl = $this->curlInit();
            curl_setopt($curl, CURLOPT_URL, $this->host . "/" . $url . '/' . $id);
            curl_setopt($curl, CURLOPT_PORT, $this->port);
            curl_setopt($curl, CURLOPT_VERBOSE, 0);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POST, 0);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Accept: application/json"));
            $json = curl_exec($curl);
            if (curl_errno($curl)) {
                throw new Exception(curl_error($curl));
            }
            $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($http_status === Response::HTTP_OK) {
                return json_decode($json, true);
            }
            return null;
        }

        private function rawPut($url, $expectedStatuses, $body)
        {
            return $this->rawPost($url, $expectedStatuses, $body, "PUT");
        }

        private function rawDelete($url, $expectedStatuses)
        {
            return $this->rawPost($url, $expectedStatuses, null, "DELETE");
        }

        private function rawPost($url, $expectedStatuses, $body, $customRequest = null)
        {
            $curl = $this->curlInit();
            curl_setopt($curl, CURLOPT_URL, $this->host . "/" . $url);
            curl_setopt($curl, CURLOPT_PORT, $this->port);
            curl_setopt($curl, CURLOPT_VERBOSE, 0);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT,2);
            curl_setopt($curl, CURLOPT_TIMEOUT,5);

            if ($customRequest) {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $customRequest);
            }
            curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-length: " . strlen($body)));
            $json = curl_exec($curl);
            if (curl_errno($curl)) {
                throw new Exception(curl_error($curl));
            }
            $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $list = array();
            if (!in_array($http_status, $expectedStatuses)) {
                throw new Exception("POST failed with status: " . $http_status . ", url:" . $this->host . "/" . $url . " , error: " . $json);
            }
            return json_decode($json, true);
        }

        private function curlInit()
        {
            if ($this->curl === null) {
                $this->curl = curl_init();
            } else {
                curl_reset($this->curl);
            }
            return $this->curl;
        }
    }

    class ElasticClientBulkBuilder
    {
        private $bulkChanges = [];

        public function put($id, Array $documentBody)
        {
            $this->bulkChanges[(string)$id] = [
                'type' => 'put',
                'body' => $documentBody
            ];
        }

        public function delete($id)
        {
            $this->bulkChanges[(string)$id] = [
                'type' => 'delete',
                'body' => []
            ];
        }

        public function toBatchBody($index, $type)
        {
            $bulk = "";
            foreach ($this->bulkChanges as $id => $change) {
                if ($bulk) {
                    $bulk .= "\n";
                }
                if ($change['type'] === "put") {
                    $bulk .= '{ "index" : { "_index" : "'.$index.'", "_type" : "'.$type.'", "_id" : "'.$id.'" } }'."\n";
                    $bulk .= json_encode($change['body'])."\n";
                } else {
                    $bulk .= '{ "delete" : { "_index" : "'.$index.'", "_type" : "'.$type.'", "_id" : "'.$id.'" } }'."\n";
                }
            }
            return $bulk;
        }
    }
}

namespace PSLib\Utils\Lists {

    use PSLib\Utils\Lists\Sequence;

    class Seq
    {
        public static function ofArray(Array $array)
        {
            return new Sequence($array);
        }
    }

    use PhpCollection\Sequence as PhpSeekuence;

    class Sequence extends PhpSeekuence
    {
        public function iter(\Closure $action)
        {
            foreach ($this->elements as $element) {
                $action($element);
            }
        }

        public function merge()
        {
            $newElements = array();
            foreach ($this->elements as $element) {
                $newElements = array_merge($newElements, $element);
            }
            return $this->createNew($newElements);
        }

        public function unique()
        {
            $this->elements = array_unique($this->elements);
            return $this;
        }

        public function uniqueBy($unique)
        {
            $uniques = [];
            foreach ($this->elements as $element) {
                $val = (string)$unique($element);
                if (!isset($uniques[$val])) {
                    $uniques[$val] = $element;
                }
            }
            $this->elements = array_values($uniques);
            return $this;
        }

        public function groupBy($groupValueExpression)
        {
            $groupIndexes = [];
            foreach ($this->elements as $element) {
                $val = (string)$groupValueExpression($element);
                if (!isset($groupIndexes[$val])) {
                    $groupIndexes[$val] = [];
                }
                $groupIndexes[$val][] = $element;
            }
            $groups = [];
            foreach ($groupIndexes as $key => $value) {
                $groups[] = [
                    'key' => $key,
                    'values' => $value
                ];
            }
            $this->elements = $groups;
            return $this;
        }

        public function toArray()
        {
            return $this->elements;
        }
    }
}
