<?php

require 'common.php';

$client->createIndexAndMapping('
    {
        "index" : {
            "analysis" : {
                "analyzer" : {
                    "NorSnowball" : {
                        "type" : "snowball",
                        "language" : "Norwegian"
                    }
                }
            }
        }
    }
    ',
    '
    {
        "attachments" : {
            "properties" : {
                "id" : {"type" : "string", "index" : "not_analyzed"},
                "tenantId" : {"type" : "string", "index" : "not_analyzed"},
                "patientTenancy" : {"type" : "string", "index" : "not_analyzed"},
                "createdAt" : {"type" : "date"},
                "name" : {"type" : "string"},
                "description" : {"type" : "string"},
                "public" : {"type" : "boolean"},
                "mimeType" : {"type" : "string"},
                "fileName" : {"type" : "string", "index" : "not_analyzed"},
                "fileType" : {"type" : "string", "index" : "not_analyzed"}
            }
        }
    }
');
