<?php

require 'common.php';

use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

$numberOfBatches = 10;
$numberOfDocumentsInBatch = 10;

for ($i = 0; $i < $numberOfBatches; $i++) {
    for ($j = 0; $j < $numberOfDocumentsInBatch; $j++) {
        $data = [
            'id' => Uuid::uuid4(),
            'tenantId' => Uuid::uuid4(),
            'createdAt' => Carbon::now()->format('c'),
            'name' => 'Test Attachment',
            'description' => 'Test description',
            'public' => false,
            'type' => 'file',
            'previewable' => false,
            'mimeType' => 'text/plain',
            'fileName' => 'test.txt',
            'fileType' => 'txt',
            'size' => 10,
            'fileMetadata' => [],
            'patientTenancy' => null
        ];

        $builder->put($data['id'], $data);
    }
    $client->updateMany($builder);
}
